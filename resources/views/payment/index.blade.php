@section('pageTitle', 'Payment')

@extends('layouts.app')

@section('content')
<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Payment</h1>
    </div>

    <div class="row mt-4">
        <div class="mt-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3 bg-dark">
                    <h6 class="m-0 fw-bold text-white">Payment INV/2023/00001</h6>
                </div>
                <div class="card-body">
                    <p class="mb-0 fw-bold">Cyrilus Santio Pranata</p>
                    <p class="mt-0">ID#170030055</p>

                    <div class="table-responsive">
                        <table class="table" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Produk</th>
                                    <th>Harga</th>
                                    <th>Qty</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Laptop ASUS X454Y</td>
                                    <td>Rp. {{ number_format(3000000, 2, ',', '.') }}</td>
                                    <td>
                                        <input type="number" min="1" id="qty" class="form-control form-control-solid w-25" placeholder="1" value="1" required/>
                                    </td>
                                    <td>Rp. <span id="total">{{ number_format(3000000, 2, ',', '.') }}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <button type="submit" class="btn btn-dark mt-3 mb-4" disabled>Proses Payment</button>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('script')
    <script>
        $('#qty').on('keyup click', function() {
            var price = 3000000;
            var qty = $('#qty').val();

            var total = price * qty;
            console.log(total);
            document.querySelector('#total').innerText = total.toLocaleString('id-ID', { minimumFractionDigits: 2 });
        });
    </script>
@endsection
