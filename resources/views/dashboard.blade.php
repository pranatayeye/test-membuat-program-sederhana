@section('pageTitle', 'Dashboard')

@extends('layouts.app')

@section('content')
<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Dashboard</h1>
    </div>

    <div class="row mt-4">
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card shadow h-100 py-2 bg-dark">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col mr-2 text-white">
                            <div class="font-weight-bold text-uppercase mb-1">
                                Data Users</div>
                            <div class="h5 mb-0 fw-bold">
                                {{ count($users) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
