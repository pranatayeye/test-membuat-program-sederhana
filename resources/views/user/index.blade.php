@section('pageTitle', 'Users')

@extends('layouts.app')

@section('content')
<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Users</h1>
    </div>

    <div class="mt-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3 bg-dark">
                <h6 class="m-0 fw-bold text-white">Daftar User</h6>
            </div>
            <div class="card-body row">
                <div class="col-12 mt-md-0 mt-4 pt-md-3 pt-0 text-end">
                    <div class="btn-group">
                        <a class="btn btn-dark" href="#" id="category-filter">
                            <span class="text">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-funnel-fill" viewBox="0 0 16 16">
                                    <path d="M1.5 1.5A.5.5 0 0 1 2 1h12a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.128.334L10 8.692V13.5a.5.5 0 0 1-.342.474l-3 1A.5.5 0 0 1 6 14.5V8.692L1.628 3.834A.5.5 0 0 1 1.5 3.5v-2z"/>
                                </svg>
                            </span>
                            <span class="visually-hidden">Toggle Dropdown</span>
                        </a>
                        <select id="category-select" class="form-select">
                            <option value="">All Categories</option>
                            <option value="genap">Genap</option>
                            <option value="ganjil">Ganjil</option>
                        </select>
                    </div>
                    <a type="button" class="btn btn-dark ms-2" data-bs-toggle="modal" data-bs-target="#addUser">
                        <span class="text">Tambah User</span>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead class="bg-dark text-light">
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Pekerjaan</th>
                                <th>Tanggal lahir</th>
                                <th>Keterangan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->job }}</td>
                                    <td>{{ Carbon\Carbon::parse($user->birth_date)->format('d-m-Y') }}</td>
                                    
                                    <?php
                                        $date = Carbon\Carbon::parse($user->birth_date)->format('d-m-Y');

                                        // Ubah format tanggal menjadi format yang dapat diolah
                                        $timestamp = strtotime($date);
                                        $day = date('j', $timestamp);
                                        $weekNumber = date('W', $timestamp);

                                        // Cek apakah tanggal genap atau ganjil
                                        if ($day % 2 == 0) {
                                            $dayStatus = 'genap';
                                        } else {
                                            $dayStatus = 'ganjil';
                                        }

                                        // Cek apakah minggu genap atau ganjil
                                        if ($weekNumber % 2 == 0) {
                                            $weekStatus = 'genap';
                                        } else {
                                            $weekStatus = 'ganjil';
                                        }
                                    ?>

                                    <td>
                                        @if ($dayStatus == 'genap')
                                            <span class="badge text-bg-primary">tanggal genap</span>
                                        @else
                                            <span class="badge text-bg-warning">tanggal ganjil</span>
                                        @endif
                                        
                                        @if ($weekStatus == 'genap')
                                            <span class="badge text-bg-primary">minggu genap</span>
                                        @else
                                            <span class="badge text-bg-warning">minggu ganjil</span>
                                        @endif
                                    </td>

                                    <td class="align-middle">
                                        <div class="dropdown">
                                            <a href="#" class="dropdown-toggle card-drop text-dark"
                                                data-bs-toggle="dropdown" aria-expanded="false">
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-end">
                                                <li><a class="dropdown-item" href="#" onclick="editUser('{{$user->id}}', '{{$user->uuid}}', '{{$user->name}}', '{{$user->job}}', '{{$user->birth_date}}')">
                                                        Edit
                                                    </a>
                                                </li>
                                                <li><a class="dropdown-item" href="#" onclick="deleteUser('{{$user->uuid}}', '{{$user->name}}')">
                                                        Hapus
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<!--begin::Modal - tambah User-->
<div class="modal fade" id="addUser" tabindex="-1" aria-labelledby="addUserLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="POST" action="{{ route('users.store') }}" class="user">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                <h1 class="modal-title fs-5" id="addUserLabel">Tambah User</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="px-3">
                        <div class="form-floating mb-3">
                            <input type="text" 
                                name="name" 
                                class="form-control text-start @error('name') is-invalid @enderror" 
                                id="name" placeholder="Nama"
                                value="{{ old('name') }}" required></input>
                            <label for="floatingInput">Nama<span class="text-danger">*</span></label>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="px-3">
                        <div class="form-floating mb-3">
                            <input type="text" 
                                name="job" 
                                class="form-control text-start @error('job') is-invalid @enderror" 
                                id="job" placeholder="Pekerjaan"
                                value="{{ old('job') }}" required></input>
                            <label for="floatingInput">Pekerjaan<span class="text-danger">*</span></label>
                            @error('job')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="px-3">
                        <div class="form-floating mb-3">
                            <input type="date" 
                                class="form-control @error('birth_date') is-invalid @enderror" 
                                id="birth_date" placeholder="date" name="birth_date"
                                value="" required>
                            <label for="floatingInput">Tanggal lahir<span class="text-danger">*</span></label>
                            @error('birth_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-dark">Simpan</button>
                </div>
            </div>
        </form>
    </div>
  </div>
<!--end::Modal - tambah User-->

<!--begin::Modal - edit User-->
<div class="modal fade" id="editUser" tabindex="-1" aria-labelledby="editUserLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="POST" id="formEditUser">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                <h1 class="modal-title fs-5" id="userLabel"></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="px-3">
                        <div class="form-floating mb-3">
                            <input type="text" 
                                name="name" 
                                class="form-control text-start @error('name') is-invalid @enderror" 
                                id="nameEdit" placeholder="Nama"
                                value="" required></input>
                            <label for="floatingInput">Nama<span class="text-danger">*</span></label>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="px-3">
                        <div class="form-floating mb-3">
                            <input type="text" 
                                name="job" 
                                class="form-control text-start @error('job') is-invalid @enderror" 
                                id="jobEdit" placeholder="Pekerjaan"
                                value="" required></input>
                            <label for="floatingInput">Pekerjaan<span class="text-danger">*</span></label>
                            @error('job')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="px-3">
                        <div class="form-floating mb-3">
                            <input type="date" 
                                class="form-control @error('birth_date') is-invalid @enderror" 
                                id="birth_dateEdit" placeholder="date" name="birth_date"
                                value="" required>
                            <label for="floatingInput">Tanggal lahir<span class="text-danger">*</span></label>
                            @error('birth_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-dark">Simpan</button>
                </div>
            </div>
        </form>
    </div>
  </div>
<!--end::Modal - tambah User-->

@endsection

@section('script')
<script>
    /* datatables */
    $(document).ready(function() {
        var table = $('#dataTable').DataTable();

        // Buat elemen dropdown untuk filter kategori
        var categoryFilter = $('#category-select');

        // Filter berdasarkan kategori saat dropdown berubah
        categoryFilter.on('change', function() {
            var selectedCategory = $(this).val();
            table.column('4').search(selectedCategory).draw();
        });
    });
    /* end datatables */

    /* add User */
    @if (session()->has('successAddUser'))
        // sweetAlert
        Swal.fire({
            text: "{{ session('successAddUser') }}",
            icon: "success",
            buttonsStyling: false,
            confirmButtonText: "Oke!",
            customClass: {
                confirmButton: "btn btn-dark"
            }
        })
    @endif
    /* end add User */

    /* edit User */
    function editUser(userID, userUUID, userName, userJob, userBirth) {

        // combine
        var form = '#formEditUser' + userUUID;
        var modal = '#editUser' + userUUID;
        var label = '#userLabel' + userUUID;
        var name = '#nameEdit' + userUUID;
        var job = '#jobEdit' + userUUID;
        var birth = '#birth_dateEdit' + userUUID;

        var url = "{{ route('users.update', ':uuid') }}";
        url = url.replace(':uuid', userUUID);

        // change
        var idModal = $("#editUser").attr("id", modal);
        var idLabel = $("#userLabel").attr("id", label);
        var idName = $("#nameEdit").attr("id", name);
        var idJob = $("#jobEdit").attr("id", job);
        var idBirth = $("#birth_dateEdit").attr("id", birth);
        var idForm = $("#formEditUser").attr("id", form).attr('action', url);

        $(idLabel).text('Ubah User - ' + userName);

        $(idModal).on('shown.bs.modal', function() {
            $(idName).val(userName);
            $(idJob).val(userJob);
            $(idBirth).val(userBirth);
        });

        $(idModal).on('hidden.bs.modal', function() {
            // set null before reset
            $(idName).val("");
            $(idJob).val("");
            $(idBirth).val("");
            $(idForm).removeAttr("action");

            // reset ID
            $(idModal).attr("id", 'editUser');
            $(idLabel).attr("id", 'userLabel');
            $(idName).attr("id", 'nameEdit');
            $(idJob).attr("id", 'jobEdit');
            $(idBirth).attr("id", 'birth_dateEdit');
            $(idForm).attr("id", 'formEditUser');
        });

        $(idModal).modal('show');
    }

    @if (session()->has('successEditUser'))
    // sweetAlert
    Swal.fire({
        text: "{{ session('successEditUser') }}",
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "Oke!",
        customClass: {
            confirmButton: "btn btn-dark"
        }
    })
    @endif
    /* end edit User */

    /* delete */
    function deleteUser(userUUID, userName) {
        console.log(userUUID);
        Swal.fire({
            text: "Anda yakin ingin menghapus " + userName + "?",
            icon: "warning",
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonText: "Ya, hapus!",
            cancelButtonText: "Tidak, batal",
            customClass: {
                confirmButton: "btn fw-bold btn-danger",
                cancelButton: "ms-2 btn fw-bold btn-secondary"
            }
        }).then(function (result) {
            if (result.value) {
                var url = "{{ route('users.destroy', ':uuid') }}";
                url = url.replace(':uuid', userUUID);

                window.location.href = url;
            } else if (result.dismiss === 'cancel') {
                Swal.fire({
                    text: userName + " tidak dihapus.",
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Oke!",
                    customClass: {
                        confirmButton: "btn fw-bold btn-dark",
                    }
                });
            }
        });
    }

    @if (session()->has('successDestroyUser'))
        // sweetAlert
        Swal.fire({
            text: "Anda telah menghapus {{ session('name') }} !",
            icon: "success",
            buttonsStyling: false,
            confirmButtonText: "Oke!",
            customClass: {
                confirmButton: "btn btn-dark"
            }
        })
    @endif
    /* end delete */
</script>
@endsection