


@section('pageTitle', 'Error 404')

@extends('layouts.app')

@section('content')
<!-- 404 Error Text -->
<div class="text-center mt-5">
    <div class="error mx-auto" data-text="404">404</div>
    <p class="lead text-gray-800 mb-5">Page Not Found</p>
    <p class="text-gray-500 mb-0">The page you are trying to access is not available</p>
    <a href="javascript:history.back()">&larr; Back to the previous page</a>
</div>
@endsection

