<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="mt-3">
      <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link {{ strpos(Request::path(), '/') !== false ? 'active' : '' }}" aria-current="page" href="{{ route('dashboard') }}">
                Dashboard
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ strpos(Request::path(), 'users') !== false ? 'active' : '' }}" aria-current="page" href="{{ route('users.index') }}">
                Users
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ strpos(Request::path(), 'payment') !== false ? 'active' : '' }}" aria-current="page" href="{{ route('payment.index') }}">
                Payment
            </a>
        </li>
      </ul>
    </div>
  </nav>