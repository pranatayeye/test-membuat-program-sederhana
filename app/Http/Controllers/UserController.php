<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function index()
    {              
        return view('user.index', [
            'users' => User::latest()->get(),
        ]);
    }

    public function store(Request $request)
    {
        // return $request->all();
        $request->validate([
            'name' => ['required'],
            'job' => ['required'],
            'birth_date' => ['required', 'date'],
        ]);

        User::create([
            'uuid' => Str::uuid()->toString(),
            'name' => $request->name,
            'job' => $request->job,
            'birth_date' => $request->birth_date,
        ]);

        return redirect()->route('users.index')->with('successAddUser', 'User berhasil dibuat');
    }

    public function update(Request $request, User $users)
    {
        $request->validate([
            'name' => ['required'],
            'job' => ['required'],
            'birth_date' => ['required', 'date'],
        ]);

        // get by id
        $users = User::find($users->id);

        $users->name = $request->name;
        $users->job = $request->job;
        $users->birth_date = $request->birth_date;
        $users->save();

        return redirect()->route('users.index')->with('successEditUser', 'User berhasil diubah');
    }

    public function destroy(User $users)
    {
        User::where('uuid', $users->uuid)->delete();

        return redirect()->route('users.index')->with([
            'successDestroyUser' => 'User berhasil dihapus',
            'name' => $users->name,
        ]);
        
    }
}
