<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PaymentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/* dashboard */
Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

/* users */
Route::get('users', [UserController::class, 'index'])->name('users.index');
Route::post('users/create', [UserController::class, 'store'])->name('users.store');
Route::post('users/{users:uuid}/edit', [UserController::class, 'update'])->name('users.update');
Route::get('users/{users:uuid}/delete', [UserController::class, 'destroy'])->name('users.destroy');

/* payment */
Route::get('payment', [PaymentController::class, 'index'])->name('payment.index');

// Route::get('/redirect-payment', function () {
//     $apiKey = 'SANDBOX87CC2DFA-FB00-42AB-9051-902CBA8E1E7E'; // Ganti dengan API Key iPaymu kamu
//     // $merchantKey = 'YOUR_MERCHANT_KEY'; // Ganti dengan Merchant Key iPaymu kamu

    
//     $va = '0000007861189600';
//     //Request Body//
//     $body['product']    = array('headset', 'softcase');
//     $body['qty']        = array('1', '3');
//     $body['price']      = array('100000', '20000');
//     $body['returnUrl']  = 'https://your-website.com/thank-you-page';
//     $body['cancelUrl']  = 'https://your-website.com/cancel-page';
//     $body['notifyUrl']  = 'https://your-website.com/callback-url';
//     $body['referenceId'] = '1234'; //your reference id
//     //End Request Body//
//     $method       = 'POST';
//     //Generate Signature
//     // *Don't change this
//     $jsonBody     = json_encode($body, JSON_UNESCAPED_SLASHES);
//     $requestBody  = strtolower(hash('sha256', $jsonBody));
//     $stringToSign = strtoupper($method) . ':' . $va . ':' . $requestBody . ':' . $apiKey;
//     $signature    = hash_hmac('sha256', $stringToSign, $apiKey);
//     $timestamp    = Date('YmdHis');
//     //End Generate Signature

//     // dd($timestamp);
//     $url = 'https://sandbox.ipaymu.com/api/v2/payment';
//     $headers = [
//         'Content-Type' => 'application/json',
//         'signature' => $signature,
//     ];

//     $payload = [
//         'formdata' => [
//             'product' => ['Baju'],
//             'qty' => [1],
//             'price' => [10000],
//             'description' => ['Baju1'],
//             'returnUrl' => 'https://ipaymu.com/return',
//             'notifyUrl' => 'https://ipaymu.com/notify',
//             'cancelUrl' => 'https://ipaymu.com/cancel',
//             'referenceId' => 'ID1234',
//             'weight' => [1],
//             'dimension' => ['1:1:1'],
//             'buyerName' => 'putu',
//             'buyerEmail' => 'putu@mail.com',
//             'buyerPhone' => '08123456789',
//             'pickupArea' => '80117',
//             'pickupAddress' => 'Jakarta',
//             'paymentMethod' => 'va',
//         ]
//     ];

//     $response = Http::withOptions([
//         'verify' => false, // Jika menggunakan HTTPS, gunakan SSL verification yang sesuai
//     ])->withHeaders($headers)->post($url, $payload);

//     $data = $response->json();

//     // Proses dan tampilkan respon dari API iPaymu
//     return $data;
// });

